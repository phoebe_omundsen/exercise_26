﻿using System;

namespace exercise_26
{

    public class Person
    {
        private string Name;
        private int IdNumber;

        public Person(string _name, int _idNumber)
        {
            Name = _name;
            IdNumber = _idNumber;
        }
        public string PrintName        {
            get {var a = $"Person name = {Name}";   
                 return a;
                }  
        }
        public int SetId
        {
            set {IdNumber = value;}
        }
        public string PrintId 
        {
            get {var b = $"ID Number = {IdNumber}";
                 return b;
                }
        }  
    }
    class Program
    {
        static void Main(string[] args)
        {
            var a1 = new Person("Joe", 566849);
            Console.WriteLine(a1.PrintName);
            Console.WriteLine(a1.PrintId);
            Console.WriteLine();
            a1.SetId = 123456789;
            Console.WriteLine(a1.PrintId);
        }
    }
}
